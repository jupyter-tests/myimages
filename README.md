# myimages

Images in public domain: CC0 1.0 Universal.

* [birds.jpg](https://www.publicdomainpictures.net/fr/free-download.php?image=animaux&id=85162) - author George Hodan
* [eagle.jpg](https://www.publicdomainpictures.net/fr/free-download.php?image=animaux&id=306138) - author unknown
* [monkeys.jpg](https://www.publicdomainpictures.net/fr/free-download.php?image=animaux&id=63074) - author George Hodan
* [hyppo.jpg](https://www.wallpaperflare.com/hippo-nature-animal-world-safari-africa-hippopotamus-water-wallpaper-afmfp/download) - author unknown
